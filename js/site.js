/* On Page Load Function */
$(document).ready(function () {
  showDimensionTab();
  showColouredVehicle('silver');

  $(".banner-nav-mini").click(function () {
    $(".banner-nav").toggle();
  });


  $("#navbar-menu-btn").click(function() {
    $(".mobile-nav-menu").toggle();
  })
});

/* Show specific div based on selected tab (for vehicle specification div) */
function showDimensionTab() {
  removeActiveBtnClass("#layout-btn, #colour-btn, #interior-trim-btn");
  hideSpecTabs("#veh-layout-container, #veh-colour-container, #veh-interior-trim-container");
  addActiveBtnClass("#dimensions-btn");
  $("#veh-dimensions-container").show();
}

/* Show specific div based on selected tab (for vehicle specification div) */
function showLayoutTab() {
  removeActiveBtnClass("#dimensions-btn, #colour-btn, #interior-trim-btn");
  hideSpecTabs("#veh-colour-container, #veh-dimensions-container, #veh-interior-trim-container");
  addActiveBtnClass("#layout-btn");
  $("#veh-layout-container").show();
}

/* Show specific div based on selected tab (for vehicle specification div) */
function showColourTab() {
  removeActiveBtnClass("#layout-btn, #dimensions-btn, #interior-trim-btn");
  hideSpecTabs("#veh-layout-container, #veh-interior-trim-container, #veh-dimensions-container");
  addActiveBtnClass("#colour-btn");
  $("#veh-colour-container").show();
}

/* Show specific div based on selected tab (for vehicle specification div) */
function showInteriorTrimTab() {
  removeActiveBtnClass("#layout-btn, #dimensions-btn, #colour-btn");
  hideSpecTabs("#veh-layout-container, #veh-colour-container, #veh-dimensions-container");
  addActiveBtnClass("#interior-trim-btn");
  $("#veh-interior-trim-container").show();
}

/* Generic function to remove active class from button tab  */
function removeActiveBtnClass(button) {
  $(button).removeClass("active-secondary-btn");
}

/* Generic function to add active class from button tab  */
function addActiveBtnClass(button) {
  $(button).addClass("active-secondary-btn");
}

/* Generic function to hide vehicle spec tab details  */
function hideSpecTabs(tab) {
  $(tab).hide();
}

/* Car selector function: Show correct colour car based on selected swatch  */
function showColouredVehicle(colour) {
  if (colour === 'silver') {
    $("#vehicle-img").attr('src', '../assets/img/cars/Silver.png')
  }

  if (colour === 'metalicious') {
    $("#vehicle-img").attr('src', '../assets/img/cars/Metalicious.png')
  }

  if (colour === 'red') {
    $("#vehicle-img").attr('src', '../assets/img/cars/Red.png')
  }

  if (colour === 'magnetic') {
    $("#vehicle-img").attr('src', '../assets/img/cars/Magnetic.png')
  }

  if (colour === 'guard') {
    $("#vehicle-img").attr('src', '../assets/img/cars/Guard.png')
  }

  if (colour === 'solar') {
    $("#vehicle-img").attr('src', '../assets/img/cars/Solar.png')
  }

  if (colour === 'deep-impact-blue') {
    $("#vehicle-img").attr('src', '../assets/img/cars/DeepImpactBlue.png')
  }

  if (colour === 'black') {
    $("#vehicle-img").attr('src', '../assets/img/cars/Black.png')
  }

  if (colour === 'blazer-blue') {
    $("#vehicle-img").attr('src', '../assets/img/cars/BlazerBlue.png')
  }
}